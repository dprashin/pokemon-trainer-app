# Name of the React App is POKEMON-TRAINER-APP

# DATABASE: https://dd-dp-n-api.herokuapp.com/trainers

# HEROKU LINK: https://dd-dp-pkmn-trainer.herokuapp.com/

# Authors:  
 
 Devendra P. and Dominik  Drat

# DESCRIPTION:
- The App starts when an user signs in and clicks on the submit button.
- The name of the User is stored to the Trainers API
- Once the name has been stored in the API, the app redirects to pokemon catalogue page.
- Local storage is used to manage the session. 
- In Pokemon catalogue page, various pokemon are displayed with use of PokeAPI. User is able to click on a pokemon to make it "collected"
- Trainer page displays collected pokemon. User is able to remove them from the collection by clicking on "Remove" button.
- The user who is logged in is shown in the Navbar.
- When user logs out of the trainer page he is directed to the login page.


# VISUALS
The Component tree is used to show the structure and features of the component.

# Getting Started with ng new

This project was bootstrapped with [ng new]


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:4200/](http://localhost:4200/) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
