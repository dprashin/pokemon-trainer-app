import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map, switchMap, retry, finalize, tap, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { User } from "../models/user.model";
import { Pokemon } from "../models/pokemon.model";
import { SessionService } from "./session.service";

const TRAINERS_API_URL = environment.trainersAPI;

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public attempting: boolean = false;
    public error: string = '';



    constructor(
        private readonly http: HttpClient,
        private readonly session: SessionService) {

    }

    private findByUsername(username: string): Observable<User[]> {
        return this.http.get<User[]>(`${TRAINERS_API_URL}?username=${username}`)
    }

    private createUser(username: string, pokemon: Pokemon[]): Observable<User> {
        const headers = new HttpHeaders()
            .set('content-type', 'application/json')
            .set('X-API-Key', environment.trainersAPIKey)

        return this.http.post<User>(`${TRAINERS_API_URL}`, { username, pokemon }, { "headers": headers })
    }

    private patchUser(id: number, pokemon: Array<string>): Observable<User> {
        const headers = new HttpHeaders()
            .set('content-type', 'application/json')
            .set('X-API-Key', environment.trainersAPIKey)
        return this.http.patch<User>(`${TRAINERS_API_URL}/${id}`, { pokemon }, { "headers": headers })
    }

    public UpdateUser(id: number, pokemons: Array<string>, onSuccess: () => void): void {
        this.patchUser(id, pokemons)
            .subscribe(
                (user: User) => {
                    if (user.id) {
                        this.session.setUser(user)
                        onSuccess()
                    }
                },
                (error: HttpErrorResponse) => {
                    this.error = error.message
                }

            )
    }

    public authenticate(username: string, onSuccess: () => void): void {
        this.attempting = true;
        this.findByUsername(username)
            .pipe(
                retry(2),
                switchMap((users: User[]) => {
                    if (users.length) {
                        return of(users[0])
                    }
                    const pokemon: Array<Pokemon> = [];
                    return this.createUser(username, pokemon)
                }),
                finalize(() => {
                    this.attempting = false;
                })
            )
            .subscribe(
                (user: User) => {
                    if (user.id) {
                        this.session.setUser(user)
                        onSuccess();
                    }
                },
                (error: HttpErrorResponse) => {
                    this.error = error.message;
                }
            )
    }

}