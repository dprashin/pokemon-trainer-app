import { Injectable } from '@angular/core';
import { Pokemon } from "../models/pokemon.model";
import {PokemonResponse} from "../models/pokemon-response.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";

const { pokeAPI } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private readonly pokemonCache$: any;
  public pokemon: Pokemon[] = [];
  public error: string = '';
  public collectedPokemon: Pokemon[] = [];


  constructor(private readonly http: HttpClient) {
    this.pokemonCache$ = this.http
      .get<PokemonResponse>(`${pokeAPI}?limit=251`)
  }

  public fetchPokemon(): void {
    this.pokemonCache$
      .pipe(
        map((response: any) => {
          return response.results.map((pokemon: Pokemon) => ({
            ...pokemon,
            ...this.getIdAndAvatar(pokemon.url),
          }));
        })
      )
      .subscribe(
        (pokemon: Pokemon[]) => {
          this.pokemon = pokemon;
          //localStorage.setItem('pokemonCollection', JSON.stringify(pokemon.map(p => p)))
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  private getIdAndAvatar(url: string): any {
    const id = Number(url.split('/').filter(Boolean).pop());
    return {
      id,
      avatar: `https:///raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
      collected: false
    };
  }
}
