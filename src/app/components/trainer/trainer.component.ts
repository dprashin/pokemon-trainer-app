import { Component, OnInit } from '@angular/core';
import {Pokemon} from "../../models/pokemon.model";
import {SessionService} from "../../services/session.service";
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  public trainerName: string = '';
  public pokemonCollection: Pokemon[] = []
  public trainerPokemons: string[] = [];

  constructor(
    private readonly sessionService: SessionService,
    private readonly pokemonService: PokemonService
  ) {}

  ngOnInit(): void {
    this.trainerName = this.sessionService.user.username;
    this.trainerPokemons = this.sessionService.user.pokemon;

    for (let pokemonName of this.trainerPokemons) {
      for (let pok of this.pokemonService.pokemon) {
        if (pokemonName === pok.name) {
          pok.collected = true;
          this.pokemonCollection.push(pok);
        }
      }
    }
  }
}
