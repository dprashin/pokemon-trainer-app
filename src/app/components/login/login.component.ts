import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly sessionService: SessionService
  ) {

  }

  ngOnInit(): void {
    if(this.sessionService.user !== undefined) {
      this.router.navigate(['catalogue'])
    }
  }

  onSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value;
    this.userService.authenticate(username, async () => {
      await this.router.navigate(['catalogue'])
    })
  }

  get attempting(): boolean {
    return this.userService.attempting;
  }
}
