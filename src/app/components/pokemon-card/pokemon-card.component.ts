import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector: 'app-pokemon-card-component',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent {
  constructor(
    private readonly userService: UserService,
    private readonly pokemonService: PokemonService
  ) {

  }
  @Input() pokemon: Pokemon;

  public isInCollection: boolean

  ngOnInit(): void {
    this.isInCollection = this.pokemon.collected;
  }

  onToggle(pokemon: Pokemon) {
    let localUser = JSON.parse(localStorage.getItem("user"));
    let tmpPokemon = localUser["pokemon"];

    this.isInCollection = !this.isInCollection;
    this.pokemon.collected = !this.pokemon.collected;

    if (this.isInCollection) {
      tmpPokemon.push(pokemon.name);
      this.pokemonService.collectedPokemon.push(this.pokemon)
      //this.isInCollection = true;
    }

    if (!this.isInCollection) {
      const index = tmpPokemon.indexOf(pokemon.name);
      const indexTwo = this.pokemonService.collectedPokemon.indexOf(this.pokemon)
      if (index > -1) {
        tmpPokemon.splice(index, 1);
        this.pokemonService.collectedPokemon.splice(indexTwo, 1)
      }

    }
    this.userService.UpdateUser(localUser["id"], localUser["pokemon"], async () => {
    });
  }
}
