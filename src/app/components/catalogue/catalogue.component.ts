import { Component, OnInit } from '@angular/core';
import { PokemonService } from "../../services/pokemon.service";
import { Pokemon } from "../../models/pokemon.model";
import {SessionService} from "../../services/session.service";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  public offset: number = 0;
  public limitPerPage: number = 18;
  public trainerPokemons: string[] = [];

  constructor(private readonly pokemonService: PokemonService,
              private readonly sessionService: SessionService) {}

  public get pokemon(): Pokemon[] {
    for (let pokemonName of this.trainerPokemons) {
      for (let pok of this.pokemonService.pokemon) {
        if (pokemonName === pok.name) {
          pok.collected = true;
        }
      }
    }
    return this.pokemonService.pokemon.slice(
      this.offset,
      this.offset + this.limitPerPage
    );
  }

  ngOnInit(): void {
    this.trainerPokemons = this.sessionService.user.pokemon;
    this.pokemonService.fetchPokemon();
  }

  public previousPage(): void {
    this.offset -= this.limitPerPage;
  }

  public nextPage(): void {
    this.offset += this.limitPerPage;
  }
}
